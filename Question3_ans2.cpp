#include <stdio.h>
//Answer 2 without arrays

int main() {
	
    int num1,x = 0, rem; //declaring 3 integer variables
    
    printf("Enter the Number: ");
    scanf("%d", &num1); //scans the number from user
    
    for(;num1 >0;num1/=10){ //loop to reverse the number, while the num1 greater than zero num1 will divided by 10 in every cycle.
    
    	rem=num1%10; //after devided by 10 remainder is stored in rem variable
    	x=x*10+rem; //x is used to create reveresed number by multiplying itself by 10 and adding remainder to it.
    	
	}
	
    printf("Reversed Number = %d", x);
    
    return 0;
}
