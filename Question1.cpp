#include<stdio.h>
#include<conio.h>
int main(){
	
	int num1=0,num2=0;
	
	for(int i=1;;i++){//infinite loop with integer i
	
		printf("You can exit by entering 0 or minus value\n");
		printf("Enter your Number %i : ",i); //integer i is used to display count
		scanf("%i",&num1); //integer num1 is the number to be add
		
		if(num1>0){ //if num1 is greater than 0 loop will continue
		
			num2=num2+num1;
			
		}else{//otherwise loop wont continue anymore
		
			break;
		}
		
	}
	
	printf("Sum of all given Numbers = %i",num2);//if user entered a negative value or 0 final answer will be printed.
	
	return 0;
}
