#include<stdio.h>
//Answer 1 with arrays

int main(){
	
	int num1,i=0,k=0; //declaring 3 integer variables
	
	printf("Enter the Number: ");
	scanf("%i",&num1);  //get the input form user to num1
	
	int y=num1; 
	
	for(i;y>=10;i++){ //loop to find size of the number that user entered.
	
		y=y/10;
		
	}
	
	int arr[i+1]; //(i+1) is the size of number and its used to create a array called arr to store reversed number
	
	for(i;i>=0;i--){//loop to store the reversed number in array arr.
	
		arr[k]=num1%10; //remainderb after divided by 10.
		num1=num1/10; //this removes entered number's last digit in every cycle.
		k++; //used to input remainder to array arr using its address via k
		
	}
	
	printf("Reversed Number = ");
	
	for(int l=0;l<k;l++){ //after above process we can print the array arr to display reversed number.
	
		printf("%i",arr[l]); 
	}
	
    return 0;
}
