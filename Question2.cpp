#include<stdio.h>
#include<conio.h>
int main(){
	
	int number=0,X=0;
	
	printf("Enter the positive number that you want to check :");
	scanf("%i",&number); //to take user input
	
	for(int i=2;i<=number/2;i++){ //entered number is going to divide by 2 to its half value
	
		if(number%i==0){//if reminder gets 0 value of X will change into 1
		
			X=1;
			break;
			
		}
		
	}
	
	if(number==1){ //1 is neither prime nor composite number.
	
		printf("%i is not a Prime Number",number);
		
	}else if(X==0){ //if X is still 0, it means its a prime.
	
		printf("%i is a prime Number",number);
		
	}else{ //if X is changed it means the number you entered is not a prime.
	
		printf("%i is not a Prime Number",number);
		
	}
	
	return 0;
}
