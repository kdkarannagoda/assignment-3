#include<stdio.h>
int main(){
	int num1;
	
	printf("Enter the Number: ");
	scanf("%i",&num1); //scans the number from user
	printf("Factors of %i is/are: ",num1);
	
	for(int i =1 ;i<=num1;i++){
		
		if(num1%i==0){ //if any number is divisible by (1 to its value) without remainders, it should be a factor to that number.
		
			printf("\t%i",i);
			
		}
		
	}
	
	return 0;
}
