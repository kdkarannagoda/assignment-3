#include<stdio.h>
int main(){
	
	int num1,ans;
	
	printf("Enter the Number: ");
	scanf("%i",&num1);//scans the number from user
	
	for(int i=1;i<16;i++){//loop starts with 1 and end when it reachs 15 so multiplication table will continue till 15.
	
		ans=i*num1; //ans is calculated using i and the number that user entered in every cycle.
		printf("\n%i\tx\t%i\t=\t%i",i,num1,ans);
		
	}
	
	return 0;
}
